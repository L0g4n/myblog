
all: build

# production build
build:
	zola build

# serve content with local dev server
serve:
	zola serve

serve-drafts:
	zola serve --drafts

spellcheck:
	find . -name "*.md" -exec hunspell -d en_US -t -i utf-8 '{}' \;

