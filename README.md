# My personal blog

This repo contains the source code for my personal blog, currently deployed at [blog.l0g4n.me](https://blog.l0g4n.me).

The site is powered by the static site generator [Zola](https://getzola.org).

## Build instructions

Download Zola and run `zola build` for a production build, or `zola serve` for a development build served by a local development server.

