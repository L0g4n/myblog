+++
title = "About This Blog"
path  = "about"
+++

My Name is Yannik and this is my personal blog. I'm from Germany and studied Applied Computer Science at the University of Bamberg and IT security at the University of Applied Sciences in Munich.

This blog is *mostly* about topics related to programming, IT related stuff, and maybe other things I think I absolutely need to tell you about.

Check out my [GitLab](https://gitlab.com/L0g4n) profile to get an overview of what I've been doing or what I am currently working on.

**The opinions presented here are my own and do not represent the opinion of any employer or any other formal organization.**
