+++
title =     "We need a Vetting Process in Public Registries for Supply Chain Security"
date  =      2022-02-18T18:35:00+01:00
draft = true

[taxonomies]
tags       = ["IT Security", "Supply Chain Security", "Dynamic Analysis"]

#[extra]
#toc = true
+++

Most software developers know that today's software is only getting more complex. And with complex I don't mean complexity as in
a requiring a higher cognitive load but rather more bloated as more and more dependencies are added to sofware projects.
This is directly a result of the success of open source software.

There are so many libraries out there available for free which are incredibly
useful for saving time and costs, e.g. for building quickly web applications or kickstarting your fancy new ML project in Python.
Don't misunderstand me, this is great since it empowers practically anyone with little coding abilities to start something new or just getting the job done much faster. Of course you don't want to spend your valuable time while your making a new game figuring out how to generate random numbers. Just pick and library, like [rand](https://github.com/rust-random/rand) and move on!

However, for every blessing there's usually also a curse. Every dependency you add to your code also increases its attack surface. Meaning, there's only need to be **one** vulnerability (intentional or unintentional) and all your other systems can get compromised. Or, as this obligatory [xkcd][xkcd-2347] explains it better:

![Dependency](https://imgs.xkcd.com/comics/dependency.png)

The real problem, however, is rather how we pull in also those dependencies. Usually, those libraries are all pulled from the internet. In most instances, from a big centralized registry which hosts the code and offers a service for uploading and downloading your software to the platform. The big names are here, for example, **npm** for JavaScript, **RubyGems** for Ruby, **crates.io** for Rust, and **Pub.dev** for .NET, to name just a few.

Again, everybody can upload arbitrary code to each registry (in theory, at least) which again is fully in-line with the open development spirit. As these centralized instances represent a single point of failure it is of paramount importance that they are properly secured.
Even if you trust the developers of a given project, you may **not the trust the build artifacts** which are downloaded onto your computer and injected in your codebase. After all, even if only the credentials of one authorized developer are compromised, he can easily just push one malicious version and, if you are unlucky, you pull in that compromised verson you get from the registry.

**I want to argue in this post that most registries could do more to secure the open source ecosystem**.

## Background

[xkcd-2347]: https://xkcd.com/2347/
