+++
title =     "The pain of using budget notebooks as a software developer"
date  =     2022-05-25T23:04:49+02:00

#[taxonomies]
#tags       = ["budget laptops", "company notebooks"]

#[extra]
#toc = true
+++

## Words of warning

I have thought a long a long time whether I should post this, as some people might interpret this post as a a typical *first world problem* and an incredibly privileged problem to complain about since many people have no access to technology at all. I am aware of that, however, I still decided to make this post as it's just an accurate description of one particular thing I am experiencing at work.

## Background

I'm currently pursuing my master's in IT security full time; additionally I'm working part-time as a working student at Siemens in the cybersecurity department in the security vulnerability monitoring team as software developer primarily to earn some money on the side.

In short, Siemens offers a service called **Vilocify** to other companies where they can register all of their so-called *components* (this includes all software and hardware) and assign them to different *monitoring lists*. Then, as soon as a new vulnerability for a component gets created, the customers get notified through dedicated channels (web portal, email) to inform. Ultimately, the selling point of this service to manage cybersecurity in your organization and having an overview about all vulnerabilities that affect your organization (click [here](https://new.siemens.com/global/en/products/services/cybersecurity/vilocify.html) if you want to read more about the Vilocify service).

My job as a software developer is working on the Vilocify portal which is basically a web UI for managing all the data needed to run that service. This includes creating components, creating notifications for new vulnerabilities, and so on. As you can probably image, this web service follows a pretty traditional architecture and consists of a backend and a frontend. However, there is no clean separation between both of them; it's one giant code base: A Ruby on Rails app weirdly interwoven with an [obscure JavaScript framework][extjs]. It's a weird code base as parts of the web UI are rendered on the server from the Ruby on Rails app and the rest rendered on the client via the JS framework.

## The pain of using under-powered notebooks

OK, that ought to be enough background information, let's get back to topic. The company notebook I am working with is a ThinkPad L490 with an i5-8265U and 32GB of RAM. And what can I say... It sucks working with that thing.
Slow would be an understatement to describe the development experience. Don't get me wrong, this machine is probably perfectly fine for normal office tasks like writing emails, browsing the web, creating presentations, and so on. However, for developing it is under-powered, at least in my specific work environment.

Let me explain that more in detail. The required operating system to use is of course Microsoft Windows, as it is the norm in most of german companies. However, we can't really develop on the managed Windows installation, as you don't have permissions to install anything. That's why the official dev environment is running Ubuntu in a virtual machine (typically VMware). 

**And precisely that ruins any situation where quick feedback loops are required**.

The notebook is simply too slow to handle any graphical Linux distribution with acceptable latency. When I boot the VM, I can go for a coffee break as it already takes one minute alone to even boot up. The coding experience in RubyMine, the default IDE, is also horrible, as the launching of the project takes three minutes alone to index all required files. That alone wouldn't be too bad if at the least editing code would not regularly lag and freeze the whole editor.

And, btw: It's not just the Linux VM which is slow. The laptop can't seem to handle Microsoft Teams (yes, I know, its abhorrent) on its own, as you can watch the incremental rendering process unfold every time you open a chat.

## Brought to you by ingenious cost savings of (german) corporations

I accept that - since I am still a student - my working experience is rather limited. I've only worked in two companies so far: Siemens and Infineon Technologies. Both are big public companies in the DAX (the most important german stock market index). However, I have heard that many mid tier companies or even startups offer better hardware to their software engineers, some of them even let their employees freely choose.

**Why is that even companies with much less capital offer better hardware to their employees?**

It's counter-intuitive, right? Should it not be exactly the opposite? I mean, usually the more money you have, the more you can spend. That's why students usually are not able to buy a car since they earn much less than a regular working employee. My hypothesis in this matter is that this mainly done due to cost savings (of course). Many public companies (at least in Germany) want to maximize their profit. That's why they buy a huge quantity of usually one model of a notebook from a single OEM (maybe a few variants) and dump them on their employees without considering their requirements. Thus, they can get a good deal from the manufacturer including a considerable quantity discount (hello intel!).

## What about you?

Maybe you as a reader have made similar experiences as I did (or haven't). If you are working in tech how satisfied are your with your equipment provided by your company? I'm open for your input.

[vilocify]: https://new.siemens.com/global/en/products/services/cybersecurity/vilocify.html
[extjs]: https://www.sencha.com/products/extjs/
